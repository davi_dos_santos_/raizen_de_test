FROM puckel/docker-airflow:1.10.7

USER root

ENV PYTHONPATH=/usr/local/airflow
RUN \
 apt-get update -y && \
 apt-get install -y --no-install-recommends \
	pkg-config \
	libpq-dev \
	libsnappy-dev \
	\
	libxml2-dev \
	libxmlsec1-dev \
	xmlsec1 \
 	build-essential \
	libsnappy-dev \
	libz-dev \
	liblz-dev \
	liblz4-dev \
	libbz2-dev

COPY airflow.cfg /usr/local/airflow/airflow.cfg
COPY entrypoint.sh /entrypoint.sh

ADD requirements.txt /usr/local/airflow/

RUN pip install --upgrade pip==20.2 && pip install -r /usr/local/airflow/requirements.txt

RUN pip install pandas-gbq \
	gspread oauth2client pandas pandas-gbq mysql-connector-python pymssql \
	--upgrade google-auth \
	--upgrade google-cloud-bigquery \
	google-cloud-storage \
	paramiko \
	sshtunnel \
	fastparquet \
	pyarrow \
	gspread_dataframe

ADD dags /usr/local/airflow/dags


