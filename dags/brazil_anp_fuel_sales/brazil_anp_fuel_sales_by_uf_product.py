import pandas as pd
import sqlalchemy
from datetime import datetime, timedelta
from airflow import DAG
from airflow.operators.python_operator import PythonOperator
import os

db = sqlalchemy.create_engine('sqlite:///raizen.db')


def extract_and_load():
    file_name = os.path.abspath(os.getcwd()) + '/dags/brazil_anp_fuel_sales/artefacts/vendas-combustiveis-m3-tipo1.xls'
    dfs = pd.read_excel(file_name, sheet_name=None)

    for sheet_name in dfs.keys():
        df_sheet = dfs[sheet_name]
        df_sheet = df_sheet.drop(['REGIÃO', 'TOTAL'], axis=1)
        df_table = pd.melt(df_sheet, id_vars=['COMBUSTÍVEL', 'ANO', 'ESTADO'], var_name="MES", value_name="TOTAL_MES")
        series = df_table.groupby(['COMBUSTÍVEL', 'ANO', 'ESTADO', 'MES']).TOTAL_MES.sum()
        series.to_sql('sales_oil_derivative_fuels_by_uf_and_product', con=db, if_exists='append')

    print('>>> extract_and_load done!')


yesterday = datetime.now() - timedelta(days=1)
default_args = {
    'owner': 'airflow',
    'start_date': datetime(yesterday.year, yesterday.month, yesterday.day)
}

dag = DAG('brazil_anp_fuel_sales_by_uf_product',
          schedule_interval='0 8 * * *',
          default_args=default_args,
          catchup=False)

PythonOperator(
        task_id='extract_and_load',
        python_callable=extract_and_load,
        dag=dag
)
